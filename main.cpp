#include "Vector.h"
#include <iostream>

void display(Vector& other)
{
    for (int i = 0; i < other.size(); i++)
    {
        std::cout << other[i] << " ";
    }
    std::cout << '\n';
}

int main()
{
    double* array = new double[3];
    for (int i = 0; i < 3; i++)
    {
        array[i] = i + 1;
    }
    Vector Vector2;
    Vector Vector1;
    Vector2 = Vector1;
    
    Vector2.pushBack(58);
    display(Vector2);
    for (int i = 0; i < 5; i++)
    {
        Vector2.pushBack(i);
    }
    display(Vector2);
    
    Vector2.pushFront(123);
    display(Vector2);
    for (int i = 0; i < 5; i++)
    {
        Vector2.pushFront(i);
    }
    display(Vector2);
    
    /*Vector2.insert(23, 1);
    display(Vector2);
    
    Vector2.insert(array, 3, 3);
    display(Vector2);

    Vector2.popBack();
    display(Vector2);
    
    Vector2.popFront();
    display(Vector2);*/
    
    Vector2.erase(1, 4);
    display(Vector2);

    Vector2.eraseBetween(5, 8);
    display(Vector2);

    std::cout << Vector2.loadFactor() << std::endl;
    Vector2.find(58);
    Vector2.find(2);
    Vector2.find(-1432);
    Vector2.find(752);
    delete[] array;
    return 0;
}
