.PHONE: all clear run

all: main

main: main.o Vector.o
	g++ $^ -o Vector -g -fsanitize=address -fsanitize=undefined

%.o: %.cpp
	g++ $< -o $*.o -c -g -fsanitize=address -fsanitize=undefined 

clear:
	rm -f *.o Vector

run: main
	./Vector
