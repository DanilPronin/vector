#include <iostream>
#include "Vector.h"

Vector::Vector(const Value* rawArray, const size_t size, float coef)
{
    _data = new Value[size];
    for (size_t i = 0; i < size; i++)
    {
        _data[i] = rawArray[i];
    }
    _size = size;
    _capacity = size;
    _multiplicativeCoef = coef;
}

Vector::Vector(const Vector& other)
{
    this->~Vector();
    this->_size = other._size;
    this->_capacity = other._size;
    this->_multiplicativeCoef = other._multiplicativeCoef;
    this->_data = new Value[_size];
    for (size_t i = 0; i < _size; i++)
    {
       this->_data[i] = other._data[i];
    }
}
Vector& Vector::operator=(const Vector& other)
{
    this->~Vector();
    this->_size = other._size;
    this->_capacity = other._size;
    this->_multiplicativeCoef = other._multiplicativeCoef;
    if (!other._data)
    {
        return *this;
    }
    this->_data = new Value[_size];
    for (size_t i = 0; i < _size; i++)
    {
       this->_data[i] = other._data[i];
    }
    return *this;
}

Vector::Vector(Vector&& other) noexcept
{
    this->_size = other._size;
    other._size = 0;
    this->_capacity = other._capacity;
    other._capacity = 0;
    this->_multiplicativeCoef = other._multiplicativeCoef;
    other._multiplicativeCoef = 2.0f;
    this->_data = other._data;
    other._data = nullptr;
}
Vector& Vector::operator=(Vector&& other) noexcept
{
    if (this == &other)
    {
        return *this;
    }
    this->_size = other._size;
    other._size = 0;
    this->_capacity = other._capacity;
    other._capacity = 0;
    this->_multiplicativeCoef = other._multiplicativeCoef;
    other._multiplicativeCoef = 2.0f;
    this->_data = other._data;
    other._data = nullptr;
    return *this;
}

Vector::~Vector()
{
    delete[] _data;
    _data = nullptr;
    _size = 0;
}

void Vector::pushBack(const Value& value)
{
    if (_capacity <= 0)
    {
        _capacity = _multiplicativeCoef;
        _data = new Value[_capacity];
    }
    if (_capacity <= _size)
    {
        _capacity *= _multiplicativeCoef;
        Value* array = new Value[_capacity];
        for (size_t i = 0; i < _size; i++)
        {
            array[i] = _data[i];
        }
        delete[] _data;
        _data = array;
    }
    //_size++; /появляется странный нолик в середине, не уверен, нужна ли эта строчка
    _data[_size++] = value;
}

void Vector::pushFront(const Value& value)
{
    if (_capacity <= 0)
    {
        _capacity = _multiplicativeCoef;
        _data = new Value[_capacity];
    }
    if (_size >= _capacity)
    {
        _capacity *= _multiplicativeCoef;
        Value* array = new Value[_capacity];
        for(size_t i = 0; i < _size; i++)
        {
            array[i] = _data[i];
        }
        delete[] _data;
        _data = array;
    }
    for (size_t i = 0; i < _size; i++)
    {
        _data[_size - i] = _data[_size - i - 1];
    }
    _data[0] = value;
    _size++;
}

void Vector::insert(const Value& value, size_t pos)
{
    if (_capacity <= 0)
    {
        _capacity = _multiplicativeCoef;
        _data = new Value[_capacity];
    }
    if (_capacity <= _size)
    {
        _capacity *= _multiplicativeCoef;
        Value* array = new Value[_capacity];
        for (size_t i = 0; i < _size; i++)
        {
            array[i] = _data[i];
        }
        delete[] _data;
        _data = array;
    }
    for (size_t i = 0; i < (_size - pos); i++)
    {
        _data[_size - i] = _data[_size - i - 1];
    }
    _data[pos] = value;
    _size++;
}

void Vector::insert(const Value* values, size_t size, size_t pos)
{
    if (_capacity <= 0)
    {
        _capacity = _multiplicativeCoef;
        _data = new Value[_capacity];
    }
    if ((_size + size) > _capacity)
    {
        while((_size + size) > _capacity)
        {
            _capacity *= _multiplicativeCoef;
        }
        Value* array = new Value[_capacity];
        for (size_t i = 0; i < _size; i++)
        {
            array[i] = _data[i];
        }
        delete[] _data;
        _data = array;
    }
    for (size_t i = 0; i <= (_size - 1 - pos); i++)
    {
        _data[size + _size - 1 - i] = _data[_size - 1 - i];
    }
    for (size_t i = 0; i < size; i++)
    {
        _data[i + pos] = values[i];
    }
    _size += size;
}

void Vector::insert(const Vector& vector, size_t pos)
{
    insert(vector._data, vector._size, pos);
}

void Vector::popBack()
{
    if (_size <= 0)
    {
        throw std::invalid_argument("Invalid Parametrs");
    }
    _size -= 1;
}

void Vector::popFront()
{
    if (_size <= 0)
    {
        throw std::invalid_argument("Invalid Parameters");
    }
    for (size_t i = 0; i < _size - 1; i++)
    {
        _data[i] = _data[i + 1];
    }
    _size -= 1;
}

void Vector::erase(size_t pos, size_t count)
{
    if (count > _size - pos)
    {
        for (size_t i = pos; i < _size - (_size - pos); i++)
        {
            _data[i] = _data[_size - pos + i];
        }
        _size -= (_size - pos);
    }
    else
    {
        for (size_t i = pos; i < count; i++)
        {
            _data[i] = _data[count + i];
        }
        _size -= count;
    }
}

void Vector::eraseBetween(size_t beginPos, size_t endPos)
{
    erase(beginPos, (endPos - beginPos));
}

size_t Vector::size() const
{
    return (_size);
}

size_t Vector::capacity() const
{
    return (_capacity);
}

double Vector::loadFactor() const
{
    return ((double)_size / (double)_capacity);
}

Value& Vector::operator[](size_t idx)
{
    return (_data[idx]);
}
const Value& Vector::operator[](size_t idx) const
{
    return (_data[idx]);
}

long long Vector::find(const Value& value) const
{
    size_t key = 0;
    for (size_t i = 0; i < _size; i++)
    {
        if (_data[i] == value)
        {
            key = i;
            break;
        }
        else
        {
            key = -1;
        }
    }
    if (key != -1)
    {
        std::cout << "Число найдено в массиве" << std::endl;
    }
    else 
    {
        std::cout << "Число в массиве не найдено" << std::endl;
    }
    return key;
}

void Vector::reserve(size_t capacity)
{
    if (capacity <= _capacity)
    {
        return;
    }
    Value* array = new Value[capacity];
    for (size_t i = 0; i < _size; i++)
    {
        array[i] = _data[i];
    }
    delete[] _data;
    _data = array;
    _capacity = capacity;
}

void Vector::shrinkToFit()
{
    Value* array = new Value[_size];
    for (size_t i = 0; i < _size; i++)
    {
        array[i] = _data[i];
    }
    delete[] _data;
    _data = array;
    _capacity = _size;
}

Vector::Iterator::Iterator(Value* ptr)
{
    this->_ptr = ptr;
}

Value& Vector::Iterator::operator*()
{
    return *_ptr;
}

const Value& Vector::Iterator::operator*() const
{
    return *_ptr;
}

Value* Vector::Iterator::operator->()
{
    return this -> _ptr;
}

const Value* Vector::Iterator::operator->() const
{
    return this -> _ptr;
}

Vector::Iterator Vector::Iterator::operator++()
{
    _ptr++;
    return *this;
}

Vector::Iterator Vector::Iterator::operator++(int)
{
    _ptr++;
    return *this;
}

bool Vector::Iterator::operator==(const Iterator& other) const
{
    return (this -> _ptr == other._ptr);
}

bool Vector::Iterator::operator!=(const Iterator& other) const
{
    return (this -> _ptr != other._ptr);
}

Vector::Iterator Vector::begin()
{
    Vector::Iterator example(_data);
    return example;
}

Vector::Iterator Vector::end()
{
    Vector::Iterator example(_data + _size);
    return example;
}

